using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Object = UnityEngine.Object;

namespace Managers
{
    public class CharactersPool : IDisposable
    {
        private readonly Transform _poolRoot;
        private readonly string _characterPrefabPath = "Characters/Character.prefab";
        private readonly AsyncOperationHandle<GameObject> _prefabLoaderHandle;
        private readonly List<GameObject> _cachedCharacters;

        public CharactersPool()
        {
            _prefabLoaderHandle = Addressables.LoadAssetAsync<GameObject>(_characterPrefabPath);
            _poolRoot = new GameObject("[Characters Pool]").transform;
            _cachedCharacters = new List<GameObject>();
        }

        public GameObject Get()
        {
            GameObject character = null;
            
            if (_cachedCharacters.Count > 0)
            {
                character = _cachedCharacters[0];
                character.gameObject.SetActive(true);
                _cachedCharacters.RemoveAt(0);
                return character;
            }

            character = Object.Instantiate(_prefabLoaderHandle.Result, _poolRoot);
            
            return character;
        }

        public void Destroy(GameObject character)
        {
            character.SetActive(false);
            character.transform.SetParent(_poolRoot);
            
            _cachedCharacters.Add(character);
        }

        public void Dispose()
        {
            Addressables.Release(_prefabLoaderHandle);
        }
    }
}
